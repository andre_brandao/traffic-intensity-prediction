# Traffic Intensity Prediction

This notebook contains a study for comparison between artificial feed-forward neural networks and long short term memory network for traffic intensity prediction.

## Requirements

This software makes use of the following packages:

* Numpy.
* Matplotlib.
* Pandas.
* Keras
* Scikit-Learn.
